package workers

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/profbiss/tester"
)

func CompareJSON(dataKey string) tester.WorkerFunc {
	return func(t *testing.T, data *tester.Data) {
		got, err := data.Get(dataKey)
		if err != nil {
			t.Fatalf("Can't get data from key '%s', error: %v", dataKey, err)
		}
		gotBytes, err := json.Marshal(got)
		if err != nil {
			t.Fatalf("Can't marshal data from key '%s', error: %v", dataKey, err)
		}

		if *data.Flags.Update {
			var out = new(bytes.Buffer)
			json.Indent(out, gotBytes, "", "\t")
			data.Write(dataKey, "json", out)
			return
		}

		var gotClean interface{}
		if err := json.Unmarshal(gotBytes, &gotClean); err != nil {
			t.Fatalf("Can't unmarshal data from key '%s', error: %v", dataKey, err)
		}

		var want interface{}
		wantBytes, err := data.GetFile(dataKey)
		if err != nil {
			t.Fatalf("Can't open goldenfile '%s', error: %v", dataKey, err)
		}

		if err := json.Unmarshal(wantBytes, &want); err != nil {
			t.Fatalf("Can't unmarshal content from file '%s', error: %v", dataKey, err)
		}

		assert.Equal(t, want, gotClean)
	}
}
