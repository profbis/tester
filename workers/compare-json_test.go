package workers

import (
	"encoding/json"
	"testing"

	"gitlab.com/profbiss/tester"
)

func sum(a, b int) (out int) { return a + b } // testable function

func TestSum(t *testing.T) {
	tester.NewPipeline(
		func(t *testing.T, data *tester.Data) {
			input, err := data.Get("input") // get data for make arguments
			if err != nil {
				t.Fatalf(err.Error())
			}

			var args struct {
				A, B int
			}
			// Unmarshal arguments
			if err := json.Unmarshal(input.([]byte), &args); err != nil {
				t.Fatalf("Can't unmarshal content from file")
			}

			// Run testable function
			output := sum(args.A, args.B)

			// Save results
			data.Set("output", output)
		},
		// Compare result and golden state, or update golden state
		CompareJSON("output"),
	).Run(t)
}
