package tester

import (
	"testing"
)

type Worker interface {
	Work(t *testing.T, data *Data)
}

type WorkerFunc func(t *testing.T, data *Data)

func (f WorkerFunc) Work(t *testing.T, data *Data) {
	f(t, data)
}
