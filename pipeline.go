package tester

import (
	"flag"
	"os"
	"testing"
)

const (
	defaultPipelinePath = "testdata"
)

var (
	updateFlag = flag.Bool("update", false, "update golden file or not")
)

type pipelineSettings struct {
	Path string
}

type Pipeline struct {
	workers   []WorkerFunc
	settings  *pipelineSettings
	skipShort bool
}

func NewPipeline(workers ...WorkerFunc) *Pipeline {
	if !flag.Parsed() {
		flag.Parse()
	}
	return &Pipeline{
		workers: workers,
	}
}

func (p *Pipeline) SkipShort() *Pipeline {
	p.skipShort = true
	return p
}

func (p *Pipeline) Run(t *testing.T) {
	if testing.Short() && p.skipShort {
		t.Skip("skipping test in short mode.")
	}
	subDir := t.Name()
	p.settings = &pipelineSettings{
		Path: defaultPipelinePath + "/" + subDir,
	}
	fInfo, err := os.Stat(p.settings.Path)
	if err != nil {
		t.Errorf("Can't get file '%s' info, error: %v", p.settings.Path, err)
		return
	}
	if fInfo.IsDir() {
		p.testDir(t, p.settings.Path)
	} else {
		t.Fatalf("Running tests on a single file is not supported yet")
	}

}

func (p *Pipeline) testDir(t *testing.T, path string) {
	t.Helper()
	dataSets, err := newDataSet(path, flags{Update: updateFlag})
	if err != nil {
		t.Fatalf("No data sets")
	}
	for data := range dataSets.getIterator() {
		data := data
		t.Run(data.getIndex(), func(t *testing.T) {
			for _, worker := range p.workers {
				worker.Work(t, data)
			}
		})
	}
}
