package tester_test

import (
	"gitlab.com/profbiss/tester"
	"testing"
)

func TestShort(t *testing.T) {
	tester.NewPipeline(
		func(t *testing.T, data *tester.Data) {
			if testing.Short() {
				t.Fatal("Don`t run this in short mode")
			}
		},
	).SkipShort().Run(t)
}
