package tester

import (
	"errors"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

type goldenFile struct {
	path      string
	index     string
	name      string
	extension string
}

func newGoldenFile(path, name string) *goldenFile {
	index := strings.SplitN(name, "_", 2)[0]
	filename := strings.Split(strings.Replace(name, index+"_", "", -1), ".")
	extension := filename[len(filename)-1]
	goldenName := strings.Replace(strings.Split(filename[0], extension)[0], index+"_", "", -1)

	return &goldenFile{path, index, goldenName, extension}
}

func (g goldenFile) filePath() string {
	return g.path + "/" + g.index + "_" + g.name + "." + g.extension
}

type dataSet struct {
	path string
	sets map[string]*Data
}

func newDataSet(path string, flags flags) (*dataSet, error) {
	set := dataSet{path, make(map[string]*Data)}
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}

	for _, f := range files {
		file, err := os.Open(path + "/" + f.Name())
		if err != nil {
			return nil, errors.New("can't open file")
		}

		res, err := ioutil.ReadAll(file)
		if err != nil {
			return nil, errors.New("can't read file")
		}
		file.Close()

		f := newGoldenFile(path, f.Name())
		if set.sets[f.index] == nil {
			goldenData := make(map[string]*goldenFile)
			goldenValues := make(map[string]interface{})
			set.sets[f.index] = &Data{path: path, index: f.index, values: goldenValues, metadata: goldenData, Flags: flags}
		}

		set.sets[f.index].loadData(f.name, res, *f)
	}
	return &set, nil
}

func (d *dataSet) getIterator() <-chan *Data {
	c := make(chan *Data, 1)
	go func() {
		for _, v := range d.sets {
			c <- v
		}
		close(c)
	}()
	return c
}

type flags struct {
	Update *bool
}

type Data struct {
	path     string
	index    string
	values   map[string]interface{}
	metadata map[string]*goldenFile
	Flags    flags
}

func (d Data) getIndex() string {
	return d.index
}

func (d Data) Get(key string) (interface{}, error) {
	if val, ok := d.values[key]; ok {
		return val, nil
	}
	return nil, errors.New("can't encode data from key " + key)
}

func (d Data) GetFile(key string) ([]byte, error) {
	var fileData []byte
	if val, ok := d.metadata[key]; ok {
		file, err := ioutil.ReadFile(val.filePath())
		if err != nil {
			return nil, err
		}
		fileData = file
	} else {
		return nil, errors.New("can't get file meta from key " + key)
	}
	return fileData, nil
}

func (d *Data) Set(key string, val interface{}) {
	d.values[key] = val
}

func (d *Data) Write(key, ext string, r io.Reader) {

	if _, ok := d.metadata[key]; !ok {
		d.metadata[key] = &goldenFile{
			path:      d.path,
			index:     d.getIndex(),
			name:      key,
			extension: ext,
		}
	}

	f, err := os.OpenFile(d.metadata[key].filePath(), os.O_WRONLY|os.O_CREATE|os.O_TRUNC, os.ModePerm)
	if err != nil {
		log.Fatalf("Can't open file '%s', error: %v", key, err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Printf("Can't close file '%s', error: %v", key, err)
		}
	}()

	if _, err := io.Copy(f, r); err != nil {
		log.Printf("Can't write to file '%s', error: %v", key, err)
	}
}

func (d *Data) loadData(key string, data []byte, meta goldenFile) {
	d.values[key] = data
	d.metadata[key] = &meta
}
