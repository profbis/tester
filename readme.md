# Test example
###### pkg/tester/workers/compare-json_test.go
```go
package workers

import (
	"encoding/json"
	"fmt"
    "gitlab.com/profbiss/tester"
    "gitlab.com/profbiss/tester/workers"
    "testing"
)

func sum(a, b int) (out int) { return a + b } // testable function

func TestSum(t *testing.T) {
	tester.NewPipeline(
		func(t *testing.T, data *tester.Data) {
			input, err := data.Get("input") // get data for make arguments
			if err != nil {
				t.Fatalf(err.Error())
			}

			var args struct {
				A, B int
			}
            // Unmarshal arguments 
			if err := json.Unmarshal(input.([]byte), &args); err != nil {
				t.Fatalf("Can't unmarshal content from file")
			}   
			
			// Run testable function 
			output := sum(args.A, args.B)

            // Save results
			data.Set("output", output)
		},
		// Compare result and golden state, or update golden state
		workers.CompareJSON("output"),
	).Run(t)
}

```

# Example using:
This command run tests, compare result and golden files
```bash
go test ./pkg/tester/...
```

Verbose mode, detailed by test and inputs
```bash
go test ./pkg/tester/... -v
```
Example result
```bash
?       github.com/HeliosCompliance/foundation/pkg/tester       [no test files]
?       github.com/HeliosCompliance/foundation/pkg/tester/data  [no test files]
=== RUN   TestSum
=== RUN   TestSum/1
--- PASS: TestSum (0.00s)
    --- PASS: TestSum/1 (0.00s)
PASS
ok      github.com/HeliosCompliance/foundation/pkg/tester/workers       0.023s

```


Next command update or create output golden files
```bash
go test ./pkg/tester/... -update
```